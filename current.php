<?php
$transport = array('foot', 'bike', 'car', 'plane');
$mode = current($transport); // $mode = 'foot';     //Return the current element in an array
$mode = next($transport);    // $mode = 'bike';
$mode = current($transport); // $mode = 'bike';
$mode = prev($transport);    // $mode = 'foot';
$mode = end($transport);     // $mode = 'plane';
$mode = current($transport); // $mode = 'plane';

var_dump($mode);
echo '<br>';

$arr = array();
var_dump(current($arr)); // bool(false)
echo '<br>';

$arr = array(array());
var_dump(current($arr)); // array(0) { }
?>
